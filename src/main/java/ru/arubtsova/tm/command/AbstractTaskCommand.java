package ru.arubtsova.tm.command;

import org.jetbrains.annotations.Nullable;
import ru.arubtsova.tm.exception.entity.TaskNotFoundException;
import ru.arubtsova.tm.model.Task;

import java.util.Optional;

public abstract class AbstractTaskCommand extends AbstractCommand {

    protected void showTask(@Nullable final Optional<Task> task) {
        Optional.ofNullable(task).orElseThrow(TaskNotFoundException::new);
        System.out.println("Id: " + task.get().getId());
        System.out.println("Name: " + task.get().getName());
        System.out.println("Description: " + task.get().getDescription());
        System.out.println("Status: " + task.get().getStatus().getDisplayName());
        System.out.println("Project Id: " + task.get().getProjectId());
        System.out.println("Start Date: " + task.get().getDateStart());
        System.out.println("Finish Date: " + task.get().getDateFinish());
        System.out.println("Created: " + task.get().getCreated());
    }

}
