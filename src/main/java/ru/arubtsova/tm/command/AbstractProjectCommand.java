package ru.arubtsova.tm.command;

import org.jetbrains.annotations.Nullable;
import ru.arubtsova.tm.exception.entity.ProjectNotFoundException;
import ru.arubtsova.tm.model.Project;

import java.util.Optional;

public abstract class AbstractProjectCommand extends AbstractCommand {

    protected void showProject(@Nullable final Optional<Project> project) {
        Optional.ofNullable(project).orElseThrow(ProjectNotFoundException::new);
        System.out.println("Id: " + project.get().getId());
        System.out.println("Name: " + project.get().getName());
        System.out.println("Description: " + project.get().getDescription());
        System.out.println("Status: " + project.get().getStatus().getDisplayName());
        System.out.println("Start Date: " + project.get().getDateStart());
        System.out.println("Finish Date: " + project.get().getDateFinish());
        System.out.println("Created: " + project.get().getCreated());
    }

}
