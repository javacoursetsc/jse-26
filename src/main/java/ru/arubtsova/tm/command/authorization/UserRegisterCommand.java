package ru.arubtsova.tm.command.authorization;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.arubtsova.tm.command.AbstractCommand;
import ru.arubtsova.tm.util.TerminalUtil;

public class UserRegisterCommand extends AbstractCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "registration";
    }

    @NotNull
    @Override
    public String description() {
        return "register a new user in task-manager.";
    }

    @Override
    public void execute() {
        System.out.println("Registration:");
        System.out.println("Enter Login:");
        @NotNull final String login = TerminalUtil.nextLine();
        System.out.println("Enter Email:");
        @NotNull final String email = TerminalUtil.nextLine();
        System.out.println("Enter Password:");
        @NotNull final String password = TerminalUtil.nextLine();
        serviceLocator.getAuthService().register(login, password, email);
        System.out.println("You have been successfully registered");
    }

}
