package ru.arubtsova.tm.exception.empty;

import ru.arubtsova.tm.exception.AbstractException;

public class EmptyUserIdException extends AbstractException {

    public EmptyUserIdException() {
        super("Error! Empty User Id...");
    }

}
