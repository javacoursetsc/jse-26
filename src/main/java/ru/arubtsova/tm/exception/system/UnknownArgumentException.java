package ru.arubtsova.tm.exception.system;

import org.jetbrains.annotations.Nullable;
import ru.arubtsova.tm.exception.AbstractException;

public class UnknownArgumentException extends AbstractException {

    public UnknownArgumentException(@Nullable final String argument) {
        super("Error! Unknown argument ``" + argument + "``...");
    }

}
