package ru.arubtsova.tm.exception.authorization;

import ru.arubtsova.tm.exception.AbstractException;

public class AccessDeniedException extends AbstractException {

    public AccessDeniedException() {
        super("Error! Access Denied...");
    }

}
