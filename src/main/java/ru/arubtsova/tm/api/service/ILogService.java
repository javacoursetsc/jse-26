package ru.arubtsova.tm.api.service;

import org.jetbrains.annotations.Nullable;

public interface ILogService {

    void debug(@Nullable String message);

    void info(@Nullable String message);

    void command(@Nullable String message);

    void error(@Nullable Exception e);

}
